### Project only accepting patches
This project is not actively developed but *will* accept PRs

# Archlinux U Install

Install and configure archlinux has never been easier!

You can try it first with a `virtualbox`

## Prerequisites

- A working internet connection
- Logged in as 'root'

## How to get it
### With git
- Increase cowspace partition: `mount -o remount,size=2G /run/archiso/cowspace`
- Get list of packages and install git: `pacman -Sy git`
- get the script: `git clone https://StevenHugo@bitbucket.org/StevenHugo/aui`

## How to use
- FIFO [system base]: `cd <dir> && ./fifo`  
  - e.g. DOS partition, 18GB ext4 /, 2GB swap, grub2
  - Locale - 91 - en_US
- ADD_CN_REPO [for China]
- Install aui tool, e.g. yaourt `pacman -Sy yaourt`
- Create a new user and give it sudo privileges `useradd username ` `passwd username` `visudo` `username ALL=(ALL) ALL`
- LILO [the rest...]: `cd <dir> && ./lilo`
	- choose yay as default AUR helper(Trizen is out dated and discontinued)
- CUSTOM

## FIFO SCRIPT
- Configure keymap
- Select editor
- Automatic configure mirrorlist
- Create partition
- Format device
- Install system base
- Configure fstab
- Configure hostname
- Configure timezone
- Configure hardware clock
- Configure locale
- Configure mkinitcpio
- Install/Configure bootloader
- Configure mirrorlist  
- Configure root password

## LILO SCRIPT
- Backup all modified files
- Install additional repositories
- Create and configure new user
- Install and configure sudo
- Automatic enable services in systemd
- Install an AUR Helper [yaourt, packer, pacaur]
- Install base system
- Install systemd
- Install Preload
- Install Zram
- Install Xorg
- Install GPU Drivers
- Install CUPS
- Install Additional wireless/bluetooth firmwares
- Ensuring access to GIT through a firewall
- Install DE or WM [Cinnamon, Enlightenment, FluxBox, GNOME, i3, KDE, LXDE, OpenBox, XFCE]
- Install Developement tools [Vim, Emacs, Eclipse...]
- Install Office apps [LibreOffice, GNOME-Office, Latex...]
- Install System tools [Wine, Virtualbox, Grsync, Htop]
- Install Graphics apps [Inkscape, Gimp, Blender, MComix]
- Install Internet apps [Firefox, Google-Chrome, Jdownloader...]
- Install Multimedia apps [Rhythmbox, Clementine, Codecs...]
- Install Games [HoN, World of Padman, Wesnoth...]
- Install Fonts [Liberation, MS-Fonts, Google-webfonts...]
- Install and configure Web Servers
- Many More...

## ADD_CN_REPO SCRIPT
* add [archlinuxcn] to /etc/pacman.conf for China
* install archlinuxcn-keyring

## CUSTOM SCRIPT
* install tools  
  git netease-cloud-music shadowsocks-qt5